#ifndef __KEYEXPANSION_H__
#define __KEYEXPANSION_H__

#include "binaryfield.h"
#include "sbox.h"

void keyexpansion (unsigned char * key,
                   unsigned char * round_keys,
                   int num_bytes);

#endif
