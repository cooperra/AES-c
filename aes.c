#include "aes.h"

void ecb_e (unsigned char * data, int numblocks, unsigned char * key, int keylength)
{
    unsigned char * location;
    int i;

    location = data;

    for (i = 0; i < numblocks; i++) {
        encrypt (location, key, keylength);
        location += 16;
    }
}

void ecb_d (unsigned char * data, int numblocks, unsigned char * key, int keylength)
{
    unsigned char * location;
    int i;

    location = data;

    for (i = 0; i < numblocks; i++) {
        decrypt (location, key, keylength);
        location += 16;
    }
}

void cbc_e (unsigned char * data, int numblocks, unsigned char * key, int keylength, unsigned char * iv)
{
    unsigned char * location;
    unsigned char * prevblock;
    int i;
    int j;

    location = data;

    for (i = 0; i < numblocks; i++) {
        if (i == 0) {
            prevblock = iv;
        }
        else {
            prevblock = location - 16;
        }
        for (j = 0; j < 16; j++) {
            location[j] = location[j] ^ prevblock[j];
        }
        encrypt (location, key, keylength);
        location += 16;
    }
}

void cbc_d (unsigned char * data, int numblocks, unsigned char * key, int keylength, unsigned char * iv)
{
    unsigned char * location;
    unsigned char * prevblock;
    int i;
    int j;

    location = data + (16 * (numblocks -1));

    for (i = numblocks; i >= 0; i--) {
        decrypt (location, key, keylength);
        if (i == 0) {
            prevblock = iv;
        }
        else {
            prevblock = location - 16;
        }
        for (j = 0; j < 16; j++) {
            location[j] = location[j] ^ prevblock[j];
        }
        location -= 16;
    }
}

void inciv (unsigned char * iv)
{
    int i;

    iv[15] += (unsigned char) 1;
    for (i = 15; i >= 0; i--) {
        iv[i] += (unsigned char) 1;
        if (iv[i] != 0) {
            break;
        }
    }
}

void ctr (unsigned char * data, int numblocks, unsigned char * key, int keylength, unsigned char * iv)
{
    unsigned char * location;
    unsigned char stream[16];
    int i;
    int j;

    location = data;

    for (i = 0; i < numblocks; i++) {
        for (j = 0; j < 16; j++) {
            stream[j] = iv[j];
        }
        encrypt (stream, key, keylength);
        for (j = 0; j < 16; j++) {
            location[j] = location[j] ^ stream[j];
        }
        location += 16;
        inciv(iv);
    }
}
