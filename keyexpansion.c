#include "keyexpansion.h"

void rotateleft (unsigned char * bytes)
{
    unsigned char temp = 0x00;
    temp = bytes[0];
    bytes[0] = bytes[1];
    bytes[1] = bytes[2];
    bytes[2] = bytes[3];
    bytes[3] = temp;
}

void subword (unsigned char * in)
{
    int i;
    for (i = 0; i < 4; i++) {
        in[i] = sbox(in[i]);
    }
}

void keyexpansion_core (unsigned char * word, unsigned int round)
{
    unsigned int rcon;

    rotateleft(word);
    subword(word);

    rcon = power (2, round, 0x11b);
    word[0] = word[0] ^ (unsigned char) rcon;
}

void keyexpansion (unsigned char * key,
                   unsigned char * round_keys,
                   int num_bytes)
{
    int i;
    int j;
    int expansion_length;
    unsigned char temp[4];

    switch (num_bytes) {
        case 16:
            expansion_length = 176;
            break;
        case 24:
            expansion_length = 208;
            break;
        case 32:
            expansion_length = 240;
            break;
        default:
            return;
    }

    for (i = 0; i < num_bytes; i++) {
        round_keys[i] = key[i];
    }

    while (i < expansion_length) {
        for (j = 0; j < 4; j++) {
            round_keys[i+j] = round_keys[i+j-4];
        }

        if (i % num_bytes == 0) {
            keyexpansion_core(round_keys + i, (i / num_bytes) - 1);
        }
        if (num_bytes == 32 && i % num_bytes == 16) {
            subword (round_keys + i);
        }

        for (j = 0; j < 4; j++) {
            round_keys[i+j] = round_keys[i+j] ^ round_keys[i+j-num_bytes];
        }
        i += 4;
    }
}
