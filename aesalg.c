#include "aesalg.h"

void subbytes(unsigned char * block)
{
    int i;

    for (i = 0; i < 16; i++) {
        block[i] = sbox(*(block + i));
    }
}

void invsubbytes(unsigned char * block)
{
    int i;

    for (i = 0; i < 16; i++) {
        block[i] = invSbox(*(block + i));
    }
}

void addroundkey (unsigned char * block, unsigned char * round_key)
{
    int c;
    int r;

    for (c = 0; c < 4; c++) {
        for (r = 0; r < 4; r++) {
            block[c*4 + r] = block[c*4 + r] ^ round_key[c*4 + r];
        }
    }
}

void shiftrows (unsigned char * block)
{
    unsigned char temp;
    temp = block[1];
    block[1] = block[5];
    block[5] = block[9];
    block[9] = block[13];
    block[13] = temp;
    temp = block[2];
    block[2] = block[10];
    block[10] = temp;
    temp = block[6];
    block[6] = block[14];
    block[14] = temp;
    temp = block[3];
    block[3] = block[15];
    block[15] = block[11];
    block[11] = block[7];
    block[7] = temp;
}

void invshiftrows (unsigned char * block)
{
    unsigned char temp;
    temp = block[1];
    block[1] = block[13];
    block[13] = block[9];
    block[9] = block[5];
    block[5] = temp;
    temp = block[2];
    block[2] = block[10];
    block[10] = temp;
    temp = block[6];
    block[6] = block[14];
    block[14] = temp;
    temp = block[3];
    block[3] = block[7];
    block[7] = block[11];
    block[11] = block[15];
    block[15] = temp;
}

void mixcolumn (unsigned char * column)
{
    unsigned char matrix[4] = {0x02,0x03,0x01,0x01};
    unsigned char temp;
    unsigned char nc[4] = {0x00,0x00,0x00,0x00};
    int i;
    int j;

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            nc[i] = nc[i] ^ multiply(column[j], matrix[j], 0x11b);
        }
        temp = matrix[0];
        matrix[0] = matrix[3];
        matrix[3] = matrix[2];
        matrix[2] = matrix[1];
        matrix[1] = temp;
    }

    for (i = 0; i < 4; i++) {
        column[i] = nc[i];
    }
}

void mixcolumns (unsigned char * block)
{
    int i;

    for (i = 0; i < 4; i++) {
        mixcolumn(block + (i*4));
    }
}

void invmixcolumn (unsigned char * column)
{
    unsigned char matrix[4] = {0x0e,0x0b,0x0d,0x09};
    unsigned char temp;
    unsigned char nc[4] = {0x00,0x00,0x00,0x00};
    int i;
    int j;

    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            nc[i] = nc[i] ^ multiply(column[j], matrix[j], 0x11b);
        }
        temp = matrix[0];
        matrix[0] = matrix[3];
        matrix[3] = matrix[2];
        matrix[2] = matrix[1];
        matrix[1] = temp;
    }

    for (i = 0; i < 4; i++) {
        column[i] = nc[i];
    }
}

void invmixcolumns (unsigned char * block)
{
    int i;

    for (i = 0; i < 4; i++) {
        invmixcolumn(block + (i*4));
    }
}

int encrypt (unsigned char * block, unsigned char * key, int key_len)
{
    unsigned char round_keys[256];
    unsigned char * round_key_pointer;
    int num_rounds;
    int round;

    switch (key_len) {
        case 16:
            num_rounds = 10;
            break;
        case 24:
            num_rounds = 12;
            break;
        case 32:
            num_rounds = 14;
            break;
        default:
            return -1;
    }
    keyexpansion(key, round_keys, key_len);
    round_key_pointer = round_keys;

    addroundkey(block, round_key_pointer);
    round_key_pointer += 16;

    for (round = 0; round < num_rounds - 1; round++) {
        subbytes(block);
        shiftrows(block);
        mixcolumns(block);
        addroundkey(block, round_key_pointer);
        round_key_pointer += 16;
    }
    subbytes(block);
    shiftrows(block);
    addroundkey(block, round_key_pointer);
}

int decrypt (unsigned char * block, unsigned char * key, int key_len)
{
    unsigned char round_keys[256];
    unsigned char * round_key_pointer;
    int num_rounds;
    int round;

    switch (key_len) {
        case 16:
            num_rounds = 10;
            round_key_pointer = round_keys + 160;
            break;
        case 24:
            num_rounds = 12;
            round_key_pointer = round_keys + 192;
            break;
        case 32:
            num_rounds = 14;
            round_key_pointer = round_keys + 224;
            break;
        default:
            return -1;
    }
    keyexpansion(key, round_keys, key_len);

    addroundkey(block, round_key_pointer);
    round_key_pointer -= 16;
    invshiftrows(block);
    invsubbytes(block);

    for (round = 0; round < num_rounds - 1; round++) {
        addroundkey(block, round_key_pointer);
        round_key_pointer -= 16;
        invmixcolumns(block);
        invshiftrows(block);
        invsubbytes(block);
    }
    addroundkey(block, round_key_pointer);
}
