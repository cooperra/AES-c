#include <stdio.h>
#include "binaryfield.h"
#define DEBUG

int main ()
{
    unsigned int a;
    unsigned int b;
    unsigned int result;
    struct divresult divres;

    a = 24;
    b = 4;

    divres = divide (a,b);
    printf ("DIVIDE Q: %d\n", divres.quotient);
    printf ("DIVIDE R: %d\n", divres.remainder);

    result = multiply (a,b, 0x11b);
    printf ("MULTIPLY: %x\n", result);

    a = 1;
    for (a; a < 256; a++) {
        printf ("%x inv: %x\n", a, invert(a, 0x11b));
    }

    return 0;
}
